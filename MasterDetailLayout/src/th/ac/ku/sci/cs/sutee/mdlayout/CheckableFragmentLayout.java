package th.ac.ku.sci.cs.sutee.mdlayout;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Checkable;
import android.widget.FrameLayout;

public class CheckableFragmentLayout extends FrameLayout implements Checkable {

	private boolean isChecked = false;

	public CheckableFragmentLayout(Context context) {
		super(context);
	}

	public CheckableFragmentLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CheckableFragmentLayout(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public boolean isChecked() {
		return isChecked;
	}

	@Override
	public void setChecked(boolean checked) {
		isChecked = checked;
		if (isChecked) {
			setBackgroundResource(R.drawable.checked_background);
		} else {
			setBackgroundResource(android.R.color.transparent);
		}
	}

	@Override
	public void toggle() {
		isChecked = !isChecked;
	}

}
