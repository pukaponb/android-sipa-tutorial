package com.touchsi.sutee.dotdot;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

abstract public class DotListAdapter extends BaseAdapter implements ListAdapter {

	private static final class ViewHolder {
		TextView txtCoordX;
		TextView txtCoordY;
	}

	private Context mContext;

	public DotListAdapter(Context context) {
		mContext = context;
	}

	public long getItemId(int position) {
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.dot_row, parent, false);
			viewHolder.txtCoordX = (TextView) convertView
					.findViewById(R.id.txtCoordX);
			viewHolder.txtCoordY = (TextView) convertView
					.findViewById(R.id.txtCoordY);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		// update row here
		Dot dot = (Dot) getItem(position);
		viewHolder.txtCoordX.setText(String.valueOf(dot.getCoordX()));
		viewHolder.txtCoordY.setText(String.valueOf(dot.getCoordY()));
		
		return convertView;
	}

}
