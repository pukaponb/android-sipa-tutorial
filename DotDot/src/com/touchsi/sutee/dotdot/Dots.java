package com.touchsi.sutee.dotdot;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.touchsi.sutee.dotdot.db.DotOpenHelper;
import com.touchsi.sutee.dotdot.db.DotTable;

public class Dots {
	private ArrayList<Dot> dots = new ArrayList<Dot>();
	private DotOpenHelper openHelper;
	private SQLiteDatabase db;

	public interface OnDotsChangeListener {
		void onDotsChange(Dots dots);
	}

	Context mContext;

	public Dots(Context context) {
		mContext = context;
		openHelper = new DotOpenHelper(context);
		db = openHelper.getWritableDatabase();
	}

	private OnDotsChangeListener onDotsChangeListener;

	public void setOnDotsChangeListener(
			OnDotsChangeListener onDotsChangeListener) {
		this.onDotsChangeListener = onDotsChangeListener;
	}

	public void insert(Dot dot) {
		// dots.add(dot);
		ContentValues values = new ContentValues();
		values.put(DotTable.DotColumns.X, dot.getCoordX());
		values.put(DotTable.DotColumns.Y, dot.getCoordY());
		db.insert(DotTable.TABLE_NAME, null, values);
		notifyDotsChange();
	}

	public void clear() {
		// dots.clear();
		db.delete(DotTable.TABLE_NAME, null, null);
		notifyDotsChange();
	}

	private void notifyDotsChange() {
		if (this.onDotsChangeListener != null) {
			this.onDotsChangeListener.onDotsChange(this);
		}
	}

	public int size() {
		// return dots.size();
		Cursor cursor = db.query(DotTable.TABLE_NAME, null, null, null, null,
				null, null);
		int ret = cursor.getCount();
		cursor.close();
		return ret;
	}

	public Dot get(int position) {
		// return dots.get(position);
		Cursor cursor = db.query(DotTable.TABLE_NAME, null, null, null, null,
				null, null);
		cursor.moveToPosition(position);
		int coordX = cursor
				.getInt(cursor.getColumnIndex(DotTable.DotColumns.X));
		int coordY = cursor
				.getInt(cursor.getColumnIndex(DotTable.DotColumns.Y));
		cursor.close();
		return new Dot(coordX, coordY);

	}

	public void delete(int position) {
		dots.remove(position);
		notifyDotsChange();
	}

	public void edit(int position, int coordX, int coordY) {
		Dot dot = dots.get(position);
		dot.setCoordX(coordX);
		dot.setCoordY(coordY);
		notifyDotsChange();
	}
}
