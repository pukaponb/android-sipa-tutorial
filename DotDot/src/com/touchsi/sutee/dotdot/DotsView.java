package com.touchsi.sutee.dotdot;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class DotsView extends View {

	public DotsView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public DotsView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public DotsView(Context context) {
		super(context);
		init();
	}

	private void init() {
		paint = new Paint();
		paint.setColor(Color.BLUE);
	}

	public interface DotsViewDataSource {
		Dot getItem(int position);

		int getCount();
	}

	public interface OnDotsTouchListener {
		void onDotsTouch(DotsView dotsView, int coordX, int coordY);
	}

	private OnDotsTouchListener onDotsTouchListener;

	public void setOnDotsTouchListener(OnDotsTouchListener onDotsTouchListener) {
		this.onDotsTouchListener = onDotsTouchListener;
	}

	private DotsViewDataSource dataSource;
	private Paint paint;

	public void setDataSource(DotsViewDataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawColor(Color.WHITE);
		if (dataSource != null) {
			for (int position = 0; position < dataSource.getCount(); ++position) {
				Dot dot = dataSource.getItem(position);
				canvas.drawCircle(dot.getCoordX(), dot.getCoordY(), 10, paint);
			}
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch (event.getActionMasked()) {
		case MotionEvent.ACTION_DOWN:
			if (this.onDotsTouchListener != null) {
				this.onDotsTouchListener.onDotsTouch(this, (int) event.getX(),
						(int) event.getY());
			}
			return true;
		}
		return super.onTouchEvent(event);
	}

}
