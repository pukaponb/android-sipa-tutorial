package com.touchsi.sutee.dotdotprovider;

import java.util.Random;

import android.os.AsyncTask;

public class DotRandomTask extends AsyncTask<Integer, Integer, Void> {

	private Dao<Dot> mDotDao;
	private Random mGenerator = new Random();
	private boolean shouldStop = false;

	public DotRandomTask(Dao<Dot> dotDao) {
		mDotDao = dotDao;
	}
	
	@Override
	protected void onCancelled() {
		shouldStop = true;
	}

	@Override
	protected Void doInBackground(Integer... params) {
		for (int count = 0; count < params[0]; ++count) {
			Dot dot = new Dot();
			dot.setX(mGenerator.nextInt(300));
			dot.setY(mGenerator.nextInt(300));
			mDotDao.insert(dot);
			publishProgress(count + 1);
			if (shouldStop ) {
				break;
			}
		}
		return null;
	}

}
